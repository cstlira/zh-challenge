# Solution Architecture #

![Scheme](images/Architecture.png)

With docker we easily build and run images in different environments. We upload the files to S3 using the boto3 lib, so i thaught i was a good idea to containerize this part of the pipeline.

I chose to use Databricks to ingest the data since it's a very good tool for handling this kind of data and it uses the open source format *delta* which facilitates the administration of a data lake in various aspects (ACID transactions, time travel, etc.).

At the end of the pipeline we have RDS because i can run it on a AWS free-tier, here could be any other CDW like Redshift, Snowflake, Synapse or BigQuery. By the way we could use Databricks but you have to maintain a running cluster to connect and extract your data. This can be expensive.

### Requirements ###

* Docker 20.10.8
* Databricks Community Account (It's free. Please register at https://community.cloud.databricks.com/)

### How do I get set up? ###

#### Upload the CSV files to an S3 bucket.

  Export the AWS_ACCESS_KEY and AWS_SECRET_KEY for an account which has permissions only in a specific bucket (provided).

  ```
  export AWS_ACCESS_KEY=<provided_access_key>

  export AWS_SECRET_KEY=<provided_secret_key>

  docker run --env AWS_ACCESS_KEY=$AWS_ACCESS_KEY --env AWS_SECRET_KEY=$AWS_SECRET_KEY -it cstlira/zh-challenge:upload-data
  ```
  
  
#### Please log-in to your Databricks workspace and create a cluster:
 * Please go to the "Compute" tab -> Create Cluster
 * Choose any cluster name and click "Create cluster" at the top.
   
#### Upload the provided notebooks to the workspace:
   
Go to the "Workspace" tab.
Right-click your user and choose "Import".
Choose the DBC file provided in the "Databricks" folder of this repo.
 
In the notebook "1 - Ingestion" please replace the access_key and secret_key variables with the provided ones.

A good practice would be create a secret but since this is for demo purposes I'm using plain text.

In the notebook "4 - Feed RDS" replace the password in line 5 with the provided value.

Now you can run the notebooks in sequence.
 
The questions are answered in notebook "3 - Data Analysis"

For the last question I created an AWS QuickSight dashboard reading the data from AWS RDS but i need to know the mail addresses to share this.